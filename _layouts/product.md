---
layout: default
---
<article class=blobContent data-title=content>
    <h1>{{ page.nom }}</h1>
    <div class=metas>
        <ul>
            <li>Hauteur : {{ page.hauteur }}</li>
            <li>Largeur : {{ page.largeur }}</li>
            <li>Profondeur : {{ page.profondeur }}</li>
            <li>Poids : {{ page.poids }}</li>
        </ul>
    </div>

    {{ content }}
</article>