<header class=container>
  <h1>
    <a href={{ '/' | absolute_url }}># <span>{{ site.title }}</span></a>
  </h1>
</header>
