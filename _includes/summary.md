<section id="ghTree" class="ghTree" data-title="tree">
{% for produit in site.produits %}
  <article class="ghTreeItem ghTypeFile" data-title="dir">
    <h2 class="ghTreeTitle">
      <a
        class="folderLink"
        data-title="folderLink"
        href="{{ produit.url | relative_url }}"
      >
        {{ produit.nom }}
      </a>
    </h2>
    <p class="ghTreeExcerpt" data-title="fileExcerpt">
      {{ produit.description }}
    </p>
    <a
      class="ghTreeReadmore"
      title="Lire la suite de la fiche : {{ produit.nom }}"
      data-title="fileReadmoreLink"
      href="{{ produit.url }}"
      >Lire la suite de la fiche</a
    >
  </article>
{% endfor %}

</section>
