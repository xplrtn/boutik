<head>
    <meta charset="utf-8">
    <meta name="referrer" content="same-origin">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link crossorigin="anonymous" rel="stylesheet" href="{{ "/assets/main.css" | relative_url }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ page.title | default: page.title | default: site.gitlab.repository_url }}</title>
    <script src="https://js.stripe.com/v3/"></script>
</head>
