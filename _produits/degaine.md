---
layout: product
nom: Dégaine
description: Un timer à deux faces pour vos ateliers ou présentations.
hauteur: 150 mm
largeur: 100 mm
profondeur: 33 mm
poids: 195 g
image: degaine_running.845px.jpg
prix_conseil_ttc: 72
prix_mini_ttc: 24
---

<img src="./image/{{ page.image }}" alt="image {{ page.nom }}" id="big-image">
<div id="gallery">
    <ul>
        <li><a href="" class="gallery-item"><img src="./image/degaine_running.845px.jpg" width="60px"></a></li>
        <li><a href="" class="gallery-item"><img src="./image/degaine_running_blue.845px.jpg" width="60px"></a></li>
        <li><a href="" class="gallery-item"><img src="./image/degaine_running_yellow.845px.jpg" width="60px"></a></li>
        <li><a href="" class="gallery-item"><img src="./image/degaine_ended.845px.jpg" width="60px"></a></li>
        <li><a href="" class="gallery-item"><img src="./image/degaine_side.845px.jpg" width="60px"></a></li>
        <li><a href="" class="gallery-item"><img src="./image/degaine_x2_standby.845px.jpg" width="60px"></a></li>
        <li><a href="" class="gallery-item"><img src="./image/degaine_x2.845px.jpg" width="60px"></a></li>
        <li><a href="" class="gallery-item"><img src="./image/degaine_engraved.845px.jpg" width="60px"></a></li>
    </ul>
</div>

<script>
    const bigImage = document.querySelector("#big-image")
    Array.from(document.querySelectorAll(".gallery-item")).map(
        item => {
            item.addEventListener("click", event => {
                event.preventDefault()
                const newImage = item.querySelector("img").src
                bigImage.src = newImage
            })
        })
</script>

## Présentation

Dégaine est un compte à rebours électronique à deux faces conçu pour m'aider à suivre le temps lorsque je donne une conférence ou que j'anime un atelier. L'information est dupliquée de chaque côté la rendant plus facilement accessible à l'ensemble des participant⋅e⋅s.  
  
L'ensemble du projet est open source sous licence MIT et accessible sur gitlab.  
Dégaine est actuellement en pré-commande.  

## Descriptif :

- Affichage identique des 2 côtés
- Changement de couleur pour signaler 50%, 90% et 100% du temps écoulés
- Choix du côté d'affichage (les 2 par défaut)
- arduino complet accessible en ouvrant le boitier
- un unique bouton, qu'on pousse et qu'on tourne

## Prix libre

C'est vous qui allez définir le prix.
Des infos pour vous aider à vous positionner :

- 24 € TTC : c'est le prix de revient des matériaux plus les frais de port pour la France.
- 72 € TTC : ça couvre en plus le temps de fabrication : découpe, collage, ponçage de la structure bois, soudure des composants, test de l'ensemble. À ce prix, si c'est une activité à plein temps, les bénéfices me permettent de payer toutes mes factures.

## Pré-commander

Les premières livraisons sont prévues pour courant mai.

### Par carte bancaire

Fixer votre prix (TTC) dans le formulaire ci-dessous, appuyer sur le bouton "Payer" et c'est parti. Une fenêtre s'ouvrira pour vous permettre de préciser vos coordonnées et vos infos bancaires. Si tout se passe bien, vous serez ensuite redirigé vers une page indiquant le succès de la commande.

{% include payButton.html prix_conseil_ttc=page.prix_conseil_ttc prix_mini_ttc=page.prix_mini_ttc %}

### Autres moyens de paiement

Il est aussi possible de payer par virement ou par chèque (en Euros émis en France). Pour cela, contactez-moi, je vous fournirai les informations nécessaires.

- via twitter [@avernois](https://twitter.com/avernois) (mes dms sont ouverts)
- par e-mail [hello@crafting-labs.com](mailto:he%6c%6c%6f&#64;&#99;r&#97;f&#116;&#105;&#110;&#103;%2d&#108;&#97;b&#115;%2e%63%6f%6d)

